<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>txtConfirmPassword</name>
   <tag></tag>
   <elementGuidId>f2aba5f7-a5ba-4059-bdaf-312842f84c2a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'Input_ConfirmPassword']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>Input_ConfirmPassword</value>
   </webElementProperties>
</WebElementEntity>
