import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('WEB/2 TC_Login Demo Shop'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(3)

String priceKatalog = WebUI.getText(findTestObject('WEB/Transactions/lblPriceKatalog'))

println priceKatalog

String productNameKatalog = WebUI.getText(findTestObject('WEB/Transactions/lblProductNameKatalog'))

println productNameKatalog

WebUI.click(findTestObject('WEB/Transactions/btnBuyNow'))

WebUI.click(findTestObject('WEB/Transactions/btnDirectPayment'))

WebUI.click(findTestObject('WEB/Transactions/lstPaymentGateway'))

WebUI.callTestCase(findTestCase('WEB/0 TC_Bank System'), [:])

String priceNewOrder = WebUI.getText(findTestObject('WEB/Transactions/lblPriceNewOrder'))

println priceNewOrder

WebUI.verifyMatch(priceNewOrder, priceKatalog, true)

String productNameOrder = WebUI.getText(findTestObject('WEB/Transactions/lblProductNameOrder'))

println productNameOrder

WebUI.verifyMatch(productNameOrder, productNameKatalog, true)

WebUI.closeBrowser()