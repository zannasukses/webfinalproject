<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>POST Cek Tagihan</name>
   <tag></tag>
   <elementGuidId>40389b90-3c60-4674-8031-b8563185c787</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <connectionTimeout>-1</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\n  \&quot;product\&quot;: \&quot;PLN\&quot;,\n  \&quot;phone_number\&quot;: \&quot;085212312324\&quot;,\n  \&quot;nomor_rekening\&quot;: \&quot;112000162978\&quot;\n}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
   </httpHeaderProperties>
   <katalonVersion>7.7.2</katalonVersion>
   <maxResponseSize>-1</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>https://private-anon-d094bc1984-bisatopup.apiary-mock.com/tagihan/cek</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>-1</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()
WS.verifyElementPropertyValue(response, 'message', 'Cek tagihan berhasil')
WS.verifyElementPropertyValue(response, 'data.jumlah_tagihan', '137995')
WS.verifyElementPropertyValue(response, 'data.jumlah_bayar', '138995')
WS.verifyElementPropertyValue(response, 'data.admin', '2500')
WS.verifyElementPropertyValue(response, 'data.nama', 'SYAIBAN')
WS.verifyElementPropertyValue(response, 'data.periode', '201608')
WS.verifyElementPropertyValue(response, 'data.tagihan_id', '219')</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
