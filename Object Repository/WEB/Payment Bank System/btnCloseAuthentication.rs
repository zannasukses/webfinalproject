<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btnCloseAuthentication</name>
   <tag></tag>
   <elementGuidId>33ab3042-11d3-4fe4-8972-2d05e85c05e9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'btn btn-outline-primary d-none d-md-inline-block']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-outline-primary d-none d-md-inline-block</value>
   </webElementProperties>
</WebElementEntity>
