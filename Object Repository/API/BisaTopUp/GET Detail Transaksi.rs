<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>GET Detail Transaksi</name>
   <tag></tag>
   <elementGuidId>e2b8d81c-8824-4956-b8f1-538bc4891000</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <connectionTimeout>-1</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent></httpBodyContent>
   <httpBodyType></httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
   </httpHeaderProperties>
   <katalonVersion>7.7.2</katalonVersion>
   <maxResponseSize>-1</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>GET</restRequestMethod>
   <restUrl>https://private-anon-d094bc1984-bisatopup.apiary-mock.com/transaksi/detail/:id</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>-1</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()
WS.verifyElementPropertyValue(response, 'trans_id', '5816')
WS.verifyElementPropertyValue(response, 'order_num', '90')
WS.verifyElementPropertyValue(response, 'product_id', '31')
WS.verifyElementPropertyValue(response, 'product_name', 'PLN')
WS.verifyElementPropertyValue(response, 'product_detail_id', '115')
WS.verifyElementPropertyValue(response, 'no_hp', '087719888886')
WS.verifyElementPropertyValue(response, 'no_pelanggan', '112000162978')
WS.verifyElementPropertyValue(response, 'product_detail', 'PLN')
WS.verifyElementPropertyValue(response, 'harga', '311305')
WS.verifyElementPropertyValue(response, 'status_id', '5')
WS.verifyElementPropertyValue(response, 'status', 'Cancelled')
WS.verifyElementPropertyValue(response, 'created_at', '2016-07-20 06:11:01')</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
